import React, { Component } from 'react'
import { View, Text} from 'react-native';

export default class DetailScreen extends Component {
    render() {
        return (
            <View style={{flex:1}}>
                <View style={{backgroundColor:"#494D5F",height:'15%',flex:1,marginBottom:'13%'}}>
                    <Text style={{color:'white',marginHorizontal:'3%',marginTop:'20%',fontStyle:'italic'}}>Terakhir Di-Update : 15 Agustus 2020</Text>
                    <View style={{backgroundColor:'#49657b',height:'60%',marginTop:'5%',zIndex:20,marginHorizontal:'10%',borderRadius:8,shadowColor: '#000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 1,
    shadowRadius: 15,
    elevation: 4,justifyContent:'space-between'}}>
                        <Text style={{fontSize:24,marginTop:'4%',fontFamily:'Roboto',fontWeight:'bold',marginHorizontal:'6%',color:'white'}}>{this.props.route.params.prov}</Text>
                        <View style={{alignItems:'flex-end'}}>
                            <Text style={{fontSize:12,marginHorizontal:'6%',fontStyle:'italic',marginBottom:'2%',color:'white'}}>Provinsi / Province</Text>
                        </View>
                    </View>
                    </View>
                <View style={{backgroundColor:'#2d3246',height:'58%',shadowColor: '#000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 1,
    shadowRadius: 15,
    elevation: 3,zIndex:10}}>
                    <View style={{flex:1,flexDirection:'row',justifyContent:'space-between'}}>
                        <Text style={{fontSize:20,margin:'6%',color:'#e4dcf1'}}>FID</Text>
                        <View style={{alignItems:'center',justifyContent:'center'}}>
                            <Text style={{fontSize:12,margin:'6%',fontStyle:'italic',color:'#e4dcf1',justifyContent:'center'}}>{this.props.route.params.fid}</Text>
                        </View>
                    </View>
                    <View style={{borderBottomColor: '#e4dcf1', borderBottomWidth: 0.8,marginHorizontal:'7%',borderRadius:1}}></View>
                    <View style={{flex:1,flexDirection:'row',justifyContent:'space-between'}}>
                        <Text style={{fontSize:20,margin:'6%',color:'#e4dcf1'}}>Kode Provinsi</Text>
                        <View style={{alignItems:'center',justifyContent:'center'}}>
                            <Text style={{fontSize:12,margin:'6%',fontStyle:'italic',color:'#e4dcf1'}}>{this.props.route.params.kode}</Text>
                        </View>
                    </View>
                    <View style={{borderBottomColor: '#e4dcf1', borderBottomWidth: 0.8,marginHorizontal:'7%',borderRadius:1}}></View>
                    <View style={{flex:1,flexDirection:'row',justifyContent:'space-between'}}>
                        <Text style={{fontSize:20,margin:'6%',color:'#e4dcf1'}}>Kasus Positif</Text>
                        <View style={{alignItems:'center',justifyContent:'center'}}>
                            <Text style={{fontSize:12,margin:'6%',fontStyle:'italic',color:'#e4dcf1'}}>{this.props.route.params.positif}</Text>
                        </View>
                    </View>
                    <View style={{borderBottomColor: '#e4dcf1', borderBottomWidth: 0.8,marginHorizontal:'7%',borderRadius:1}}></View>
                    <View style={{flex:1,flexDirection:'row',justifyContent:'space-between'}}>
                        <Text style={{fontSize:20,margin:'6%',color:'#e4dcf1'}}>Kasus Sembuh</Text>
                        <View style={{alignItems:'center',justifyContent:'center'}}>
                            <Text style={{fontSize:12,margin:'6%',fontStyle:'italic',color:'#e4dcf1'}}>{this.props.route.params.sembuh}</Text>
                        </View>
                    </View>
                    <View style={{borderBottomColor: '#e4dcf1', borderBottomWidth: 0.8,marginHorizontal:'7%',borderRadius:1}}></View>
                    <View style={{flex:1,flexDirection:'row',justifyContent:'space-between'}}>
                        <Text style={{fontSize:20,margin:'6%',color:'#e4dcf1'}}>Kasus Kematian</Text>
                        <View style={{alignItems:'center',justifyContent:'center'}}>
                            <Text style={{fontSize:12,margin:'6%',fontStyle:'italic',color:'#e4dcf1'}}>{this.props.route.params.meninggal}</Text>
                        </View>
                    </View>
                    
                </View>
            </View>

        )
    }
}
