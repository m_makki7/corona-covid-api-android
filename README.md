# Corona Covid API Android

Nama Proyek: SanberApp2

Tema: Covid-19 Indonesia

Tujuan: Tugas akhir bootcamp React Native Mobile Apps by Sanbercode

About: Menampilkan data perkembangan kondisi Covid-19 di seluruh provinsi di Indonesia

Link API: https://api.kawalcorona.com/indonesia/provinsi/

Link Figma: https://www.figma.com/file/K8v2oramqbMZZFSCYlvdy8/Gabungan?node-id=6%3A118

Link Figma Preview: https://www.figma.com/proto/K8v2oramqbMZZFSCYlvdy8/Gabungan?node-id=6%3A118&scaling=min-zoom
                    https://www.figma.com/proto/K8v2oramqbMZZFSCYlvdy8/Gabungan?node-id=6%3A3&scaling=min-zoom

Link Gitlab: https://gitlab.com/m_makki7/corona-covid-api-android/-/tree/Gabungan

Penyusun:
Nama: Muchammad Makki & Gregorius Dimas Baskara

Email: m_makki7@yahoo.com & gregdimasbaskara@gmail.com
Instagram: @m_makki7 & @greg_dimasb